CREATE TABLE csv_data(category text, campaign text, color text, name text, city text, state text, photo_url text, date text, description text, amount_divested text, potential_divestment text)


COPY csv_data(category, campaign, color, name, city, state, photo_url, date, description, amount_divested, potential_divestment )
FROM '/tmp/data.csv' DELIMITER ',' CSV HEADER;



update csv_data set color = '#00a3cc' where category = 'Legislation';
update csv_data set color = '#cc0000' where category = 'City';
update csv_data set color = '#df9fbf' where category = 'Legal';
update csv_data set color = '#008000' where category = 'Movement';
update csv_data set color = '#bf8040' where category = 'University';



INSERT INTO csv_data(campaign, name, city, state, date, description, lat, long, weight, color, category)
VALUES('Launch', 'Interfaith Bank Boycott Campaign', 'Washington', 'D.C.', '2017-10-01', 'Interfaith Immigration Coalition launched boycott campaign of Wells Fargo and Bank of America, for their role in investing in prisons and anti-immigrant and incarceration policies, until a clean DREAM Act is passed by Congress.', 39.103118, -84.512020, 0.3, 'red', 'Movement');


INSERT INTO csv_data(campaign, name, city, state, date, description, lat, long, weight, color, category)
VALUES('Victory', '#RevoketheRainbow', 'Washington', 'D.C.', '2017-05-01', 'GetEQUAL pushed the National LGBTQ Task Force to reject corporate sponsorship from Wells Fargo across the organization, including the annual Creating Change conference. However, National LGBTQ TaskForce is retaining Wells Fargo as the presenting sponsor of the Task Force Gala-Miami.', 39.103118, -84.512020, 0.2, 'red', 'Movement');
