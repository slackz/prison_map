SELECT json_build_object('name', name, 'city', city, 'state', state, 'date', to_char(date(date), 'Month YYYY'),
                         'category', category, 'divested', amount_divested, 'color', color,
                         'latitude', lat, 'longitude', long, 'weight', weight, 'description', description,
                         'campaign_type', campaign)
FROM csv_data
ORDER BY weight DESC
